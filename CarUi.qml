import QtQuick 2.3

/**************
  1280 x 768
***************/
Item {
    id: itemRoot
    width: parent
    height: parent

    FontLoader {
        id: helveticaNeue
        source: "font/HelveticaNeueLTStd-ThEx.otf"
    }

    Rectangle {
        width:itemRoot.width
        height:itemRoot.height
        Image {
            source:"CarUi_images/back.png"
            id:back
            x:0 ; y:0
            width:itemRoot.width
            height:itemRoot.height
        }

        Image {
            source:"CarUi_images/topmenu.png"
            id:topmenu
            x:0 ; y:0
            width:itemRoot.width
            height:itemRoot.height/20.2105


            Rectangle {
                width:100
                height:topmenu.height
                id: backButton
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.topMargin: itemRoot.width/427
                anchors.leftMargin: itemRoot.width/70
                visible: false
                color: "transparent"


                Text {
                    font.pointSize: itemRoot.width/50
                    text: "<<<"
                    color: "#ffffff"
                    font.family: helveticaNeue.name
                    font.bold: true
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: itemRoot.state = "default"
                }
            }

            Text {
                id: topDate
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: itemRoot.width/128
                anchors.rightMargin: 10
                font.pointSize: itemRoot.width/70
                font.family: helveticaNeue.name
                color: "#ffffff"
                text: Qt.formatDateTime(new Date(), "dd.MM.yyyy\thh:mm") // Property'li yap sonst wird es nicht aktualisiert
                font.bold: true
            }

        }

        /***  MENU 1 ***/
        Image {
            source:"CarUi_images/menu1.png"
            id:menu1
            x:0 ; y:itemRoot.height/20.2105
            width:itemRoot.width/4.0378
            height:itemRoot.height/1.0520

            MouseArea {
                anchors.fill: parent
                onClicked: itemRoot.state = "menu1open"
            }
        }

        Text {
            id:phonetext
            x:itemRoot.width/14.88 ; y:itemRoot.height/1.1779

            text:'Phone'
            color:Qt.rgba(1, 1, 1, 1)
            font.pointSize: root.width/35
            font.family: helveticaNeue.name
        }

        Image {
            source:"CarUi_images/phonelogo.png"
            id:phonelogo
            x:itemRoot.width/22.8571 ; y:itemRoot.height/5.9534
            width:itemRoot.width/6.5306
            height:itemRoot.height/3.1735
        }

        /***  MENU 2 ***/
        Image {
            source:"CarUi_images/menu2.png"
            id:menu2
            x:itemRoot.width/4 ; y:itemRoot.height/20.2105
            width:itemRoot.width/4.0125
            height:itemRoot.height/1.0520

            MouseArea {
                anchors.fill: parent
                onClicked: itemRoot.state = "menu2open"
            }
        }

        Text {
            id:musictext
            x:itemRoot.width/3.0992 ; y:itemRoot.height/1.1779

            text:'Music'
            color:Qt.rgba(1, 1, 1, 1)
            font.pointSize: root.width/35
            font.family: helveticaNeue.name
        }

        Image {
            id:musiclogo
            source:"CarUi_images/musiclogo.png"
            x:itemRoot.width/3.6887 ; y:itemRoot.height/8.2580
            width:itemRoot.width/5.5411
            height:itemRoot.height/2.5016
        }


        /***  MENU 3 ***/
        Image {
            source:"CarUi_images/menu3.png"
            id:menu3
            x:itemRoot.width/1.9937 ; y:itemRoot.height/20.2105
            width:itemRoot.width/4.0251
            height:itemRoot.height/1.0520

            MouseArea {
                anchors.fill: parent
                onClicked: itemRoot.state = "menu3open"
            }
        }

        Text {
            id:maptext
            x:itemRoot.width/1.688 ; y:itemRoot.height/1.1779

            text:'Map'
            color:Qt.rgba(1, 1, 1, 1)
            font.pointSize: root.width/35
            font.family: helveticaNeue.name
        }

        Image {
            id:maplogo
            source:"CarUi_images/maplogo.png"
            x:itemRoot.width/1.8002 ; y:itemRoot.height/8.1702
            width:itemRoot.width/5.8715
            height:itemRoot.height/2.8235
        }


        /***  MENU 4 ***/
        Image {
            source:"CarUi_images/menu4.png"
            id:menu4
            x:itemRoot.width/1.3291 ; y:itemRoot.height/20.2105
            width:itemRoot.width/4.0251
            height:itemRoot.height/1.0520

            MouseArea {
                anchors.fill: parent
                onClicked: itemRoot.state = "menu4open"
            }
        }

        Text {
            id:settingstext
            x:itemRoot.width/1.2355 ; y:itemRoot.height/1.1779

            text:'Settings'
            color:Qt.rgba(1, 1, 1, 1)
            font.pointSize: root.width/35
            font.family: helveticaNeue.name
        }

        Image {
            id:settingslogo
            source:"CarUi_images/settingslogo.png"
            x:itemRoot.width/1.2635 ; y:itemRoot.height/5.6058
            width:itemRoot.width/5.5652
            height:itemRoot.height/3.3391
        }
    }


    states: [
        State {
            name: "menu1open"
            PropertyChanges {
                target: menu1
                width: itemRoot.width
                x:0
                visible:true
            }

            PropertyChanges {
                target: menu2
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: musictext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: musiclogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu3
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: maptext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: maplogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu4
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: settingstext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: settingslogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: backButton
                visible: true
            }
        },
        State {
            name: "menu2open"
            PropertyChanges {
                target: menu2
                width: itemRoot.width
                x: 0
                visible:true
            }

            PropertyChanges {
                target: musictext
                x:itemRoot.width/14.88
            }

            PropertyChanges {
                target: musiclogo
                x:itemRoot.width/22.8571
            }

            PropertyChanges {
                target: menu1
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: phonetext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: phonelogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu3
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: maptext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: maplogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu4
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: settingstext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: settingslogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: backButton
                visible: true
            }
        },
        State {
            name: "menu3open"
            PropertyChanges {
                target: menu3
                width: itemRoot.width
                x: 0
                visible:true
            }

            PropertyChanges {
                target: maptext
                x:itemRoot.width/14.88
            }

            PropertyChanges {
                target: maplogo
                x:itemRoot.width/22.8571
            }

            PropertyChanges {
                target: menu1
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: phonetext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: phonelogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu2
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: musictext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: musiclogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu4
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: settingstext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: settingslogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: backButton
                visible: true
            }
        },
        State {
            name: "menu4open"
            PropertyChanges {
                target: menu4
                width: itemRoot.width
                x: 0
                visible:true
            }

            PropertyChanges {
                target: settingstext
                x:itemRoot.width/14.88
            }

            PropertyChanges {
                target: settingslogo
                x:itemRoot.width/22.8571
            }

            PropertyChanges {
                target: menu1
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: phonetext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: phonelogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu2
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: musictext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: musiclogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: menu3
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: maptext
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: maplogo
                opacity: 0
                visible: false
            }

            PropertyChanges {
                target: backButton
                visible: true
            }
        },

        State {
            name: "default"
            PropertyChanges {
                target: menu1
                x:0 ;
                width:itemRoot.width/4.0378
                height:itemRoot.height/1.0520
                opacity:1
            }
            PropertyChanges {
                target: menu2
                x:itemRoot.width/4 ;
                width:itemRoot.width/4.0125
                height:itemRoot.height/1.0520
                opacity:1
            }
            PropertyChanges {
                target: menu3
                x:itemRoot.width/1.9937 ;
                width:itemRoot.width/4.0251
                height:itemRoot.height/1.0520
                opacity:1

            }
            PropertyChanges {
                target: menu4
                x:itemRoot.width/1.3291 ;
                width:itemRoot.width/4.0251
                height:itemRoot.height/1.0520
                opacity:1
            }

        }
    ]

    transitions: [
        Transition {
            from: "*"
            to: "default"

            SequentialAnimation{
                NumberAnimation { properties: "x, width"; duration: 1000; easing.type: Easing.InExpo }
                NumberAnimation { properties: "opacity "; duration: 333; easing.type: Easing.Linear }

            }
        },

        Transition {
            from: "*"
            to: "*"

            SequentialAnimation{
                NumberAnimation { properties: "opacity "; duration: 333; easing.type: Easing.Linear }
                NumberAnimation { properties: "visible "; }
                NumberAnimation { properties: "x, width"; duration: 1000; easing.type: Easing.OutExpo }

            }
        }
    ]
}
