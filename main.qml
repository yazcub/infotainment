import QtQuick 2.3
import QtQuick.Window 2.2

Window {
    id: root
    visible: true
    width: Screen.width/2
    height: Screen.height/2

    CarUi {
        id: testUi
        width: root.width
        height: root.height
    }
}
